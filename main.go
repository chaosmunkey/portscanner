package main

import (
	"flag"
	"fmt"
	"os"

	"gitlab.com/chaosmunkey/portscanner/port"
)

func main() {
	var host string
	var portsStr string
	// let's parse some command line arguments
	flag.StringVar(&host, "host", "localhost", "Host to scan.")
	flag.StringVar(&portsStr, "ports", "1-1024", "Comma-separated string of ports to scan; e.g. 21-23,80,443")
	flag.Parse()

	ports := port.ParsePorts(portsStr)

	if len(ports) < 1 {
		fmt.Fprint(os.Stderr, "List of ports to scan is empty.\n")
		os.Exit(1)
	}

	fmt.Printf("Starting to scan ports on %s\n", host)
	for a_port := range ports {
		if ret, err := port.ScanPort("tcp", host, a_port); ret && err == nil {
			ports[a_port] = "open"
		} else {
			ports[a_port] = "closed"
		}
	}

	fmt.Println(ports)
}
