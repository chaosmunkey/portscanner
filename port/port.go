package port

import (
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"time"
)

const (
	minPort uint16 = 1
	maxPort uint16 = 65535
)

/*
ConvertPort converts a string representation of a port
number into a 16 bit unsigned integer.
*/
func ConvertPort(inPort string) (uint16, error) {

	temp, err := strconv.ParseUint(inPort, 10, 16)
	if err != nil {
		return 0, err
	}
	port := uint16(temp)
	if inRange, err := portInRange(port); !inRange || err != nil {
		return 0, err
	}

	return port, nil
}

/*
ParsePorts parses a CSV of values returning a list of
converted ports.
*/
func ParsePorts(portsStr string) map[uint16]string {

	var ports map[uint16]string
	ports = make(map[uint16]string)

	// for i in [] type syntax in Python
	for _, value := range strings.Split(portsStr, ",") {

		// handle a trailing comma...
		if len(value) == 0 {
			continue
		}

		// handle the range items...
		if strings.Contains(value, "-") {
			portRange := strings.Split(value, "-")

			var min, max uint16
			var err error

			if min, err = ConvertPort(portRange[0]); err != nil {
				fmt.Fprintf(os.Stderr, "Error Encountered with minimum value: '%s'. Skipping...\n", err.Error())
				continue
			}
			if max, err = ConvertPort(portRange[1]); err != nil {
				fmt.Fprintf(os.Stderr, "Error Encountered with maximum value: '%s'. Skipping...\n", err.Error())
				continue
			}

			if min < max {
				for i := min; i < max+1; i++ {
					ports[i] = ""
				}
			} else {
				for i := min; i > max-1; i-- {
					ports[i] = ""
				}
			}
		} else {
			if port, err := ConvertPort(value); err != nil {
				fmt.Fprintf(os.Stderr, "Error Encountered with value: '%s'. Skipping...\n", err.Error())
			} else {
				ports[port] = ""
			}
		}
	}

	return ports
}

/*
ScanPort will detect if a port for a given protocol is open on a host.
*/
func ScanPort(protocol, host string, portNum uint16) (bool, error) {
	address := fmt.Sprintf("%s:%d", host, portNum)

	conn, err := net.DialTimeout("tcp", address, time.Second*1)

	if err != nil {
		return false, err
	}

	conn.Close()
	return true, nil
}

func portInRange(port uint16) (bool, error) {

	if port < minPort || port > maxPort {
		err := fmt.Sprintf("Port value '%d' is out of range.", port)
		return false, errors.New(err)
	}

	return true, nil
}
