package port

import (
	"fmt"
	"net"
	"reflect"
	"testing"
)

const (
	host     string = "localhost"
	protocol string = "tcp"
	port     uint16 = 8080
)

func TestConvertPort(t *testing.T) {
	var tests = []struct {
		input       string
		expected    uint16
		shouldError bool
	}{
		{"0", 0, true},
		{"80", 80, false},
		{"70000", 0, true},
	}

	for _, test := range tests {

		if output, err := ConvertPort(test.input); output != test.expected {
			t.Errorf("Test Failed: Expected output -> %d, got -> %d", test.expected, output)
		} else if test.shouldError && err == nil {
			t.Error("Test Failed: Expected an error but didn't get one.")
		} else if !test.shouldError && err != nil {
			t.Errorf("Test Failed: Shouldn't have thrown an error, but did: %s", err)
		}

	}
}

func TestParsePorts(t *testing.T) {
	result0 := make(map[uint16]string)
	result1 := map[uint16]string{
		1: "", 2: "", 3: "", 4: "",
	}
	result2 := map[uint16]string{
		21: "", 22: "", 23: "", 80: "", 8080: "",
	}

	var tests = []struct {
		input    string
		expected map[uint16]string
	}{
		{"", result0},
		{"0", result0},
		// simple range
		{"1-4", result1},
		// reverse range
		{"4-1", result1},
		// trailing comma
		{"1-4,", result1},
		// troublesome ranges
		{"0-1,65535-65537", result0},
		// mixture of range and singles
		{"80,21-23,8080", result2},
	}

	for _, test := range tests {
		output := ParsePorts(test.input)

		if !reflect.DeepEqual(output, test.expected) {
			t.Errorf("Test '%s' Failed: Expected map does not equal returned map.", test.input)
		}
	}
}

func TestScanPort(t *testing.T) {
	addr := fmt.Sprintf("%s:%d", host, port)
	srv, srvErr := net.Listen(protocol, addr)
	if srvErr != nil {
		t.Fatalf("Server failed to start: %s", srvErr)
	}

	testPass, errPass := ScanPort(protocol, host, port)

	if !testPass || errPass != nil {
		t.Errorf("%s/%d should be open %s", protocol, port, host)
	}

	srv.Close()

	testFail, errFail := ScanPort(protocol, host, port)

	if testFail || errFail == nil {
		t.Errorf("%s/%d shouldn't be open %s", protocol, port, host)
	}
}

// func TestScanPortClosed(t *testing.T) {
// 	retBool, errBool := ScanPort(protocol, host, port)

// 	if retBool || errBool == nil {
// 		t.Errorf("%s/%d shouldn't be open %s.", protocol, port, host)
// 	}
// }
