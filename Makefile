BINARY_NAME=portscan

GOTEST=go test


test:
	${GOTEST} -v ./... -cover

test-coverage:
	${GOTEST} -v ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out
